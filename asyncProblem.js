const fs = require('fs').promises;

async function performOperations() {

    const data = await fs.readFile('./index.json', 'utf-8');       
    const dataObject = JSON.parse(data);
    const solutionObject = dataObject;

    let topicArray = [];
    for(let index=0; index<dataObject.topics.length; index++){
        topicArray.push(dataObject.topics[index].directory_path);
    }
    
    for (let topicKey of topicArray) {

        const data = await fs.readFile(`./${topicKey}/index.json`, 'utf-8')
        const topicData = JSON.parse(data);

        const fileLocationArray = [];
        topicData.parts.forEach((content)=>{
            fileLocationArray.push(content.file_location);
        })

        let partsArray = [];
        for (let contentLocation of fileLocationArray) {
            const data = await fs.readFile(`./${contentLocation}`, 'utf-8');

            const dataObject = {};
            const fileData = data.split("\n");
            dataObject.name = fileData[0];
            dataObject.content = fileData[2];
            partsArray.push(dataObject);   
        }
        
        solutionObject.topics.forEach((value, index, topicArray)=>{
            if(value.directory_path === topicKey){
                topicArray[index].name = topicData.name;
                topicArray[index].parts = partsArray;
            }
        })
    }
    
    const finalResult = JSON.stringify(solutionObject, null, 2);
    await fs.writeFile('./output.json', finalResult);
}

performOperations();